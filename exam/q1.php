<?php

$passwords = array(
'user1' => '$2y$14$53224d4e333449.657312uWV2ko751hJnb4Lhj8zvqULuOP15y8dO',
'user2' => '$2y$14$53224d4f50c948.219496uL1riNBFTXM8ZUALa7VQIdJ1bMiULiGa',
'user3' => '$2y$14$53224d50774c25.308381ugIjl1npHtUv.eK8Q25ZEuABNmv7buOm'
);

foreach($passwords as $key => $value){
		$encrypted_pass = $value;	
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	$pass = $_POST['password'];
   		
   	if(crypt($pass, $encrypted_pass) != $encrypted_pass){
   		$message = "No record of that username password combination";
   	}else{
   		header('Location: https://www.google.ca');
   	}
	
}


?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Advanced PHP - Final Exam - Question 1 - User Login</title>

</head>
<body>

<h1>Advanced PHP Final Exam - Practical - Question 1 - User Login</h1>
<?php if(isset($message)) : ?>
	<h2><?=$message?></h2>
<?php else : ?>	
	<h2>Please log in</h2>
<?php endif; ?>

<form action="#" method="post">

	<p><label for="username">User name: </label><br /><input type="text" name="username" id="username" /></p>
	<p><label for="password">Password: </label><br /><input type="password" name="password" id="password" /></p>
	<p><input type="submit" value="Login" /></p>

</form>


<br />
<hr />
<br />


</body>
</html>
