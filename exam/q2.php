<?php

$dbh = new PDO("mysql:host=localhost;dbname=issd_class", 'web_user', 'mr1984');

$sql = 'SELECT * FROM oldb_product WHERE product_id = 6';

$query = $dbh->prepare($sql);

$query->execute();
$result = $query->fetch(PDO::FETCH_ASSOC);

function prettyString($string){
  //Replace underscore for space
    $string = str_replace('_', ' ', $string);
    
    //Capitalize words in the heading 
    $string = ucwords($string);
  
  return $string;
}

	
?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Advanced PHP - Final Exam - Question 2 - Auto Crud</title>
<style>

	form {
		width: 660px;
		margin-bottom: 100px;
	}

	label {
		display: inline-block;
		width: 150px;
		text-align: right;
		font-weight: bold;
		padding-right: 10px;
	}

	input[readonly="readonly"] {
		background-color: #fffccc;
	}

	input[type="text"], textarea {
		font-size: 18px;
		padding: 4px;
		width: 400px;
		line-height: 24px;
	}

	textarea {
		height: 100px;
	}

	input[type="submit"] {
		float: right;
		margin-right: 80px;
	}

</style>
</head>
<body>

<h1>Advanced PHP -  Final Exam - Practical - Question 2 - Auto Crud</h1>

<form action="q2.php" method="post">

<?php foreach($result as $key => $value) : ?>
	
	<?php if($key != 'created_at' && $key != 'updated_at') : ?>
		
          <?php if($key == 'description') : ?>
          		<p><label for='<?=$key?>'><?=prettyString($key)?></label>
          			<textarea name="<?=$key?>"><?=$value?></textarea>
          		</p>
        
          	<?php else : ?>	
		
				<p><label for='<?=$key?>'><?=prettyString($key)?></label>
		
				<input type='text' name='<?=$key?>'	value='<?=$value?>'	id='<?=$key?>' /></p>
		
				<?php endif; ?>
	
	<?php endif; ?>
			
<?php endforeach; ?>





<p><input type="submit" value="Update Product" /></p>

</form>


<br />
<hr />
<br />


</body>
</html>
