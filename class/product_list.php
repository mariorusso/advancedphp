<?php

require 'proj_config.php';

$dbh = new PDO("mysql:host=localhost;dbname=intro_php", 'mr_php', 'mr1984');

$sql = "SELECT prod_id, name, price FROM products";

$query = $dbh->prepare($sql);
  
$query->execute();

$result = $query->fetchAll(PDO::FETCH_ASSOC);

?><body>
  
    <?php foreach($result as $row) : ?>
      
      <p><a href="prod_detail.php?prod_id=<?=$row['prod_id']?>"><?=$row['name']?></a>, $<?=number_format($row['price'], 2) ?></p>
    


    <?php endforeach; ?>
   

</body>