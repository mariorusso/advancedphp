<?php 

function pageAllowed($page) {
  
  $allowed = array(
              'home',
              'about',
              'menu',
              'support',
              'shop'
            );
  if(in_array($page, $allowed)){
    return true;
  }else{
    return false;
  }
}

function getPage($page){  
  if(pageAllowed($page)){
    loadPage($page);
  }else{
    loadPage('error');
  }
}



function loadPage($page) {
  include 'inc/views/' . $page . '.php';
  
}   