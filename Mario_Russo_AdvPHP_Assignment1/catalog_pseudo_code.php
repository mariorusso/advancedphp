<?php
/**
  file: catalog_pseudo_code.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 08 2015
  description: Pseudo Code for the admin catalog page.  
*/

//Require the project config  

//Set title as Manage Products

//conect to database using PDO by the getPDO function

//Define the variable $searchtearm empty before search form is submited.

//Check if isset GET search

  //Sanatize get search string 

//End of GET is set

//Query the database to SELECT the product ID, product name, short description, image, category name as category 
//from products and category table, and where deleted is set to 0

//Prepare the query 

  
//Assign the parameters to params variable
  
//Execute the query passing the parameters 

//Include the adm header

//Echo the $title variable 

//Include the admin sidebar div

//Display title in the column Top 

//Loop thru the $result as $row to extract the values 

  //Echo the product ID and Product name link passing product ID
            
  //Echo the category
            
  //Echo the short description
  
  //Echo the price and link to edit passing the product ID

//END foreach loop            
 
//Include admin footer 
