<?php
/**
  file: edit_prod_pseudo_code.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 08 2015
  description: Edit Products Pseudo Code  
*/

//Set title variable to the page title. 

//require the config files that include the functions.

//set errors into false, and other error variable to empty variables.

//conect to database using PDO by the getPDO function

//Check if have Get prod_id

  //Sanatize $_GET, make sure prod id is a number. 
  
  //Query the DB so it get the information relative to the prod_id provided in GET
  
  //prepare the query
  
  //pass the parameters to the query.
  
  //Execute the query 
  
  //Assign each row into a variable to populate the form.

//check if have post.
	
  //Check if the xrsf_token is set and not diferent from te SESSION token if not die.	
  
  //Assign each POST into a variable to make the form sticky.  
  
  //Check for errors in Product ID. 

    //if not empty check for REGEX
  
  //END of check product ID.
  
  //Check for errors in Product Name. 
  
    //if not empty check for REGEX
  
  //END of check product name. 
  
  //Check for errors in Category Field. 
  
    //Elseif check if selected category == to 0 or Select a Category 
  
  //END of check category.
  
  //Check for errors in low stock Qty. 
  
  	//if not empty check for REGEX
  
  //END of check low stock qty. 
  
  //Check for errors in stock Qty. 
  
  	//if not empty check for REGEX
  
  //END of check stock qty.
  
  //Check for errors in image field. 
  
  	//if not empty check for REGEX
  
  //END of check image field.
  
  //Check for errors in Long description Name. 
  
  	//if not empty check for REGEX
  
  //END of check Long description name. 
  
  //Check for errors in Short description Name. 
  
  	//if not empty check for REGEX
  
  //END of check Short description name. 
  
  //Check for errors in Price. 
  
  	//if not empty check for REGEX
  
  //END of check price.
  
  //Check for errors in cost. 
  
  	//if not empty check for REGEX
  
  //END of check cost.

  
 
  //If NO errors
  
    
    //Assign the $_POST variable to normal variables and sanatize the variables. 
  
    //setting the checkbox featured
  
      //$featured is checked and value = 1
  
      //$featured is not checked and value=0
    
    //setting the checkbox deleted
  
      //$deleted is checked and value = 1
  
  
      //$deleted is not checked and value=0
    
    //Query the database to UPDATE the products table 
    
    //Prepare the query to database.
    
    //Set the parameters to be executed associating the prepared statement 
    //to the the variables with the values that we got from the form POST.
    
    //Execute the query.
    
    /* SELECT THE NEW PRODUCT FOR DISPLAY
    --------------------------------------------------------------------------------------*/
        
    //Query the database to SELECT every field from products WHERE the ID is the last ID.
    
    //Set the parameters to execute the Query using prod_id variable as the parameter.
    
    //execute the query with the parameter and get the result assigning the $product variable.
  
  //End of if not errors

// End of have $_post

//include the header for the admin.
        
//Echo the title variable in the breadcrumb div

//Echo the title variable in the H1
     
//Include the admin sidebar div

//If page does not have GET or POST let user know that he needs to select a product to edit.

//Check if the variable product is set
          
	//If the product variable is set, create a table and show the product information
            
	//To show the result use foreach loop trhough the $product array as key and value 
              
		//Echo the key using prettyString function and and the value
            
	//End the foreach loop

//ELSE show the form 

	//Open the form using the function FromOpen passing the method, action, and id 

	//Set a hidden field with the XSRF_TOKEN

	//Create the necessary inputs to the form using the functions. 

	//Show each error by the side of the input 

 	//Close the form 

//End the if statement 
        
// Include the admin footer. 
  