-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2015 at 09:04 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `intro_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `description`, `image`, `is_active`, `add_date`, `updated_date`, `deleted`) VALUES
(1, 'Snacks', 'We have a great variety of snacks, so you can have a quick and tastefull food.', 'snack_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(2, 'Coffee', 'Take a look at our variety so you can choose the perfect for you.', 'coffee_cup.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(3, 'Hot Beverages', 'Have a hot drink and enjoy our options to warm your day.', 'hot_choc_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(4, 'Cold Beverages', 'Milkshakes and another great options to refresh your day.', 'm_shake_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(5, 'Soups', 'More than 20 options of soups for you.', 'soup_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(6, 'Breakfast Specials', 'Start your day with a great and healthy choice.', 'breakfast_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(7, 'Sandwiches', 'The most tastefull sandwiches that you can have.', 'sand_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(8, 'Drinks', 'Special alcoholic drinks made with coffee.', 'irish_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0),
(9, 'Catering', 'Take a look at our catering options for your parties.', 'catering_icon.jpg', 1, '2015-01-21 22:43:47', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `street_1` varchar(255) DEFAULT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `email`, `password`, `first_name`, `last_name`, `street_1`, `street_2`, `city`, `province`, `postal_code`, `phone`, `created_at`, `updated_at`, `deleted`) VALUES
(34, 'mario@gmail.com', '$2y$10$54d4f0027a3446.081639un9h7no2vsnNLoFprM.4nJYSMFNs.nca', 'Mario', 'Russo', '595 River Ave.', '', 'Winnipeg', 'MB', 'R3L-0E6', '204-123-1234', '2015-02-06 16:46:58', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `prod_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `lowstock_qty` bigint(20) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `long_description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` decimal(5,2) DEFAULT NULL,
  `cost` decimal(5,2) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_id`, `category_id`, `name`, `quantity`, `lowstock_qty`, `short_description`, `long_description`, `image`, `price`, `cost`, `add_date`, `update_date`, `deleted`, `featured`) VALUES
(1, 1, 'Chicken drop (coxinha de frango)', 100, 10, 'The classic savory in The Coffee Place, a stuffed flatbread with seasoned chicken.', 'The drumstick is a Brazilian snack, also common in Portugal, the dough base made with wheat flour and chicken broth, which involves a filling made with seasoned chicken.', 'coxinha.jpg', '2.99', '1.50', '2015-02-01 23:08:36', '2015-02-08 20:01:18', 0, 1),
(2, 1, 'Chocolate chip cookie', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'chochipcookie.jpg', '1.29', '0.99', '2015-02-01 23:10:47', '2015-02-08 19:35:54', 0, 1),
(3, 1, 'Triple chocolate cookie', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'trichocookie.jpg', '1.29', '0.60', '2015-02-01 23:13:54', NULL, 0, 0),
(4, 1, 'White chocolate cookie', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'whtchocookie.jpg', '1.29', '0.60', '2015-02-01 23:20:23', NULL, 0, 0),
(5, 0, 'Chocolate dip donut', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'chocdipdonut.jpg', '1.99', '0.99', '2015-02-01 23:21:50', NULL, 0, 0),
(6, 1, 'Vanilla dip donut', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin accumsan posuere molestie.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin accumsan posuere molestie. Ut dictum est elit, ut scelerisque purus egestas in. Curabitur eget laoreet nisl. Aliquam erat volutpat.', 'vandipdonut.jpg', '1.99', '0.99', '2015-02-01 23:25:34', NULL, 0, 0),
(7, 1, 'Boston cream donut', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin accumsan posuere molestie.', 'Ut dictum est elit, ut scelerisque purus egestas in. Curabitur eget laoreet nisl. Aliquam erat volutpat.Ut dictum est elit, ut scelerisque purus egestas in. Curabitur eget laoreet nisl. Aliquam erat volutpat.', 'bcrdonut.jpg', '1.99', '0.99', '2015-02-01 23:26:49', NULL, 0, 0),
(8, 2, 'House Blend', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'hb_coffee.jpg', '1.50', '0.75', '2015-02-01 23:30:01', NULL, 0, 0),
(9, 1, 'Cheese Bread (pao de queijo)', 100, 10, 'This Brazilian snack made is a bun made of cheese and flour.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'cheesebread.jpg', '1.99', '0.99', '2015-02-01 23:32:54', '2015-02-08 19:51:11', 0, 0),
(10, 2, 'Dark Blend', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'dbcofee.jpg', '1.50', '0.75', '2015-02-01 23:37:23', NULL, 0, 0),
(11, 2, 'Decaf Blend', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'decafcofee.jpg', '1.50', '0.75', '2015-02-01 23:37:23', NULL, 0, 0),
(12, 5, '4 Cheese cream', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', '4cheesecr.jpg', '5.59', '2.75', '2015-02-01 23:37:23', NULL, 0, 0),
(13, 5, 'Broccoli cream', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'brocream.jpg', '5.59', '2.75', '2015-02-01 23:37:23', NULL, 0, 0),
(14, 5, 'Baked potato and bacon cream', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'potbacon.jpg', '5.59', '2.75', '2015-02-01 23:37:23', NULL, 0, 0),
(15, 5, 'Mushroom cream', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'mushcream.jpg', '5.59', '2.75', '2015-02-01 23:37:23', NULL, 0, 0),
(16, 4, 'Chocolate milk shake', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'chmilkshake.jpg', '3.49', '1.75', '2015-02-01 23:37:23', '2015-02-07 02:34:21', 0, 1),
(17, 4, 'Vanilla milk shake', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'vnmilkshake.jpg', '3.49', '1.75', '2015-02-01 23:37:23', NULL, 0, 0),
(18, 7, 'Cheese steak', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'cheesesteak.jpg', '8.99', '3.50', '2015-02-01 23:37:23', NULL, 0, 0),
(19, 7, 'Ham and Cheese paninni', 100, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus eget urna nec interdum. Ut vel ex consequat, aliquam diam ut, dignissim sem. Pellentesque lobortis mi eget venenatis gravida. Pellentesque volutpat consectetur est, in maximus elit plac', 'hmpanini.jpg', '8.99', '3.50', '2015-02-01 23:37:23', NULL, 0, 0),
(35, 8, 'Irish Coffee', 100, 10, 'The perfect blend of Coffee, Wiskey and Chantilly.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin accumsan posuere molestie. Ut dictum est elit, ut scelerisque purus egestas in. Curabitur eget laoreet nisl. Aliquam erat volutpat.', 'irishcoffee.jpg', '12.99', '6.00', '2015-02-02 01:33:06', NULL, 0, 0),
(36, 6, 'Egg Muffin', 100, 10, 'Egg Muffin is the best for breakfast.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin accumsan posuere molestie. Ut dictum est elit, ut scelerisque purus egestas in. Curabitur eget laoreet nisl. Aliquam erat volutpat.', 'eggmuffin.jpg', '5.69', '2.50', '2015-02-02 01:34:13', NULL, 0, 0),
(37, 6, 'Bacon Egg Muffin', 100, 10, 'Make you day better, starting it with a tasteful Bancon egg Muffin', 'The perfect toasted English muffin, with a generous amount of bacon, our unique seasoned egg and cheese. Don''t waste your time order yours now.', 'baconeggmuffin.jpg', '5.99', '2.50', '2015-02-06 02:48:42', '2015-02-07 03:39:14', 0, 1),
(38, 6, 'Sausage Egg Muffin', 100, 10, 'Make you day better, starting it with a tasteful Sausage egg Muffin.', 'The perfect toasted English muffin, with a our famous sausage meat, our unique seasoned egg and cheese. Don''t waste your time order yours now.', 'seggmuffin.jpg', '5.99', '2.50', '2015-02-06 02:50:12', NULL, 0, 0),
(39, 9, 'Meter Sub Sandwiches', 100, 10, 'Let us feed your guests, get the meter sub sandwiches to your party.', 'We do not joke this is really a one meter sandwich, You can chose the filling and we will deliver this delicious meal to you where and when you need.', 'mt_sand.jpg', '25.99', '12.50', '2015-02-06 19:12:36', NULL, 0, 0),
(40, 3, 'Hot Chocolate', 100, 10, 'Hot chocolate should always be this good!', 'A warm, chocolaty classic that''s hot, delicious, and perfect any time of day.', 'hotChocolate.jpg', '3.99', '2.00', '2015-02-07 03:19:38', NULL, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
