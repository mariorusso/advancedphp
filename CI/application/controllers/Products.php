<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Products extends CI_Controller {
  
  public function index(){
    $data['title'] = 'Products';
    $this->load->view('products', $data);
  }
  
  public function books() {
    $this->load->model('products_model');
    $result = $this->products_model->getAll();
    $data['title'] = 'Products List';
    $data['result'] = $result;
    $this->load->view('products', $data);
  }
  
  public function detail($id) {
    $this->output->enable_profiler(TRUE);
    $this->load->model('products_model');
    $result = $this->products_model->getOne($id);
    $data['title'] = 'Products Detail';
    $data['result'] = $result;
    $this->load->view('products', $data);
  }

}