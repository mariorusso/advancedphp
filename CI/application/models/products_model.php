<?php
/**
  file: My_Model.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 04 2015
  description: My Model Class
*/

class Products_model extends My_Model {
  
  protected $dbh;
  
  public function __construct() {
    parent::__construct();      
  }
  
  public function getAll() {
    $sql = 'SELECT * from book';
    $query = $this->dbh->prepare($sql);
    $query->execute();
    return $query->fetchAll(PDO::FETCH_ASSOC);
  }
  
  public function getOne($id){
    $book_id = intval($id);
     $sql = 'SELECT * from book WHERE book_id = ?';
    $query = $this->dbh->prepare($sql);
    $params = [$book_id];
    $query->execute($params);
    return $query->fetch(PDO::FETCH_ASSOC);
  }
}//End of Model 
