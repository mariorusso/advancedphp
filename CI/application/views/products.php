<!doctype html>
<html lang="en">
  <head> 
    <title><?=$title?></title>
    <meta charset="utf-8" />
    <meta name="description" content="The Coffee Place. We offer Fresh Coffee everytime, hot beverages and many quick meal options." />
    <meta name="keywords" content="coffee, brazilian coffee, quick meal" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <!-- google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,700%7cOxygen:300,700' rel='stylesheet' type='text/css'> 
    
    <!-- desktop css -->
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 601px)" href="C:/wamp/www/CI/styles/tcp_desktop.css" />
    
    <!-- mobile css -->
    <link rel="stylesheet" type="text/css" media="screen and (max-width: 601px)" href="styles/tcp_mobile.css" /> 
    
    <!-- Print css -->
    <link rel="stylesheet" type="text/css" media="print" href="styles/tcp_print.css" />  
    
    <!-- Start of CSS styles -->
    
    <!--[if IE]>
    
       <link rel="stylesheet" type="text/css" href="styles/tcp_desktop.css" />
  
    </style>
    <![endif]-->
  
    <!--[if LT IE 9]>
      <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('article');
      </script>     
      <style type="text/css">
        header, nav, footer, article{
         display:block;
        }
      </style>
    <![endif]-->
    <!-- End of CSS styles -->
    
  </head>
  <!-- End of head -->

  <!-- Start of body -->
  <body>
    <!-- Start of page wrapper -->
    <div id="wrapper">
    
      <!-- Start of header -->
      <header>
        <div id="logo">
          <a href="index.php" title="The Coffee Place Home Page"><img src="images/logo_mobile.jpg" alt="logo the coffee place" width="162" height="98"/> </a>
        </div>
        
        <!-- Start of Header Social Icons -->
        <div class="social_icons">
        
          <div class="fb"><a href="#"> </a></div>
          
          <div class="tw"><a href="#"> </a></div>
          
          <div class="ist"><a href="#"> </a></div>
        
        </div>
        <!-- End of Header Social Icons -->
      
        <!-- Start of search box -->
        <div id="search">
        
          <form 
            action="#"
            method="post"
            name="search_form"
            id="search_form"
           >
           
             <input type="text" name="search_field" id="search_field" placeholder="Search on this site..." required />
             <input type="submit" value=""/>
             
          </form>
          
        </div>
        <!-- end of search box -->
        <a href="cart.php">
          
        <div id='cart'>
              
            <img src="../../images/cartl.jpg" /> <p> Items in cart: <strong>0</strong></p>
            
        </div>
          
        </a>
        
        <div class="logout">
                      <p><a href="register.php">Register</a> | <a href="login.php">Login</a></p>
            
                  </div>
        
      </header><!-- End of header -->
      
      <!-- Start of navigation bar -->
      <nav>
      
        <div id="menubutton"><a href="#" id="menulink"> </a>
        
          <ul>
            <li><a class="" href="coffee.php" title="Read more about our coffee history">
                  COFFEE
                </a>
            </li>
            <li><a class="current" href="menu.php" title="View our full menu and specials">
                  MENU
                </a>
            </li>
            <li><a class="" href="locator.php" title="Locate the nearest The Coffee Place">
                  FIND YOUR PLACE
                </a>
            </li>
            <li><a class="" href="rewards.php" 
                   title="Learn more about our rewards              program">
                   REWARDS
                </a>
            </li>
            <li><a class="" href="franchising.php" 
                   title="Learn more about our franchising system">
                   FRANCHISING
                </a>
            </li>          
          </ul>
          
        </div>
      </nav><!-- End of the navigation bar -->      
      <!-- Top banner -->
      <div id="top_banner">
      </div>          
      <div id="breadcrumbs">
        <p><?=$title?></p>
      </div>
      
      <!-- Start of Content --> 
      <div id="content_wrapper"> 
      
       
        <!-- Menu Page Content starts  -->
        <div id="double_col">
          <div class="column_top">
            <h1><?=$title?></h1>
          </div>
        <pre> 
         <?=print_r($result)?>
        </pre>         
        </div>
        <!-- End of menu double col content -->
        
        <!-- Start of menu special itens right col --> 
        <div id="menu_special">
          <h2 style="color: #eee;">specials</h2>
          
          <div class="itens">
            <div class="itens_icon">
              <img src="../../images/coffee_cup.jpg" alt="coffee cup icon" width="100" height="150" />
            </div>
            
            <h4>Dark Blend</h4>
            
            <p> 
              The dark blend is the perfect blend of the dark and very dark roast giving you the perfect taste.
              <br /><a href="#">Learn more</a>. 
            </p>
                                                                                                                                      
          </div>
          
          <div class="itens">
            <div class="itens_icon">
              <img src="../../images/coxinha_icon.jpg" alt="coxinha icon" width="100" height="150" />
            </div>
            
            <h4>Chicken Drop</h4>
            
            <p> 
              The Chicken Drop is a very common and tastefull snack from Brazil that you can try at The Coffee Place.
              <br /><a href="#">Learn more</a>. 
            </p>
                                                                                                                                      
          </div>
          
          <div class="itens">
            <div class="itens_icon">
              <img src="../../images/combo.jpg" alt="combo icon" width="100" height="150" />
            </div>
            
            <h4>$3 Combo</h4>
            
            <p> 
              The 3 dollar combo is a great opportunity to try a tasty Brazilian snack with a great coffee.
              <br /><a href="#">Learn more</a>. 
            </p>
                                                                                                                                      
          </div>
     
        </div>
        <!-- End of menu special itens right col --> 
      
      </div>  
      <!-- End of Content -->
      
      <!-- Start of footer -->
      <footer>
        
        <div id="footer_cup"></div>
        
        <!-- Start of footer Social Icons -->
        <div class="social_icons">
        
          <div class="fb"><a href="#"> </a></div>
          
          <div class="tw"><a href="#"> </a></div>
          
          <div class="ist"><a href="#"> </a></div>
        
        </div>
        <!-- End of footer Social Icons -->
        
        <div id="nav_footer">
          
          <ul>
            <li><a href="contact.php">Contact Us</a></li>
            <li><a href="careers.php">Careers</a></li>
            <li><a href="faq.php">FAQ</a></li>
            <li><a href="#">Site Map</a></li>
          </ul>
        
        </div>
        
        <div id="copyright">
          <p>&copy;2015 The Coffee Place. All rights reserved.</p> 
        </div>        
        
        
      </footer>
      <!-- End of footer -->
    
    </div>
    <!-- End of wrapper -->
  
  </body><!-- End of body -->   
</html>