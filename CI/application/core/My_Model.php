<?php
/**
  file: My_Model.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 04 2015
  description: My Model Class
*/

class My_Model extends CI_Model {
  
  protected $dbh;
  
  public function __construct() {
    parent::__construct();
    
       $this->dbh = new PDO("mysql:host=localhost;dbname=issd_class", 'web_user', 'mr1984');
       $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
 
}//End of Model 
