<?php
require '../inc/proj_config.php';

$title = 'Checkout';

$subtotal = getSubTotal($cart);

$GST = getGST($subtotal);

$PST = getPST($subtotal);

$total = $subtotal + $GST + $PST;

if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] !== true) {
	$_SESSION['target'] = basename($_SERVER['PHP_SELF']);
	$_SESSION['error_message'] = 'You must be logged in to checkout';
	header("location: login.php");
	exit ;
}

//conect to database using getPDO function
$dbh = getPDO();

//Query de DB so get everything where email is == the same as user entered.
$sql = "SELECT first_name, 
				last_name,
				email,
				phone, 
				street_1, 
				street_2, 
				city, 
				province,
				postal_code				
				FROM customer
				WHERE customer_id = ?";

//Prepare the query to database.
$query = $dbh -> prepare($sql);

$params = array($_SESSION['user_id']);

//Execute the query.
$query -> execute($params);
$user = $query -> fetch(PDO::FETCH_ASSOC);

include 'inc/header_inc.php';
?>        
      <div id="breadcrumbs">
        <p><?=$title?></p>
      </div>
      <!-- Start of Content --> 
      <div id="content_wrapper"> 
       
        
        <div>
          
          <div class="column_top">
            <h1><?=$title?></h1>
          </div>
          <?php include 'inc/flash_messages.php' ?> 
        </div>
        <div id="checkout">
        	<div id="user_info">
        		<p class="first-name">Name: <?=$user['first_name']?> <?=$user['last_name']?></p>
        		<p class="phone">Phone: <?=$user['phone']?></p>
        		<p class="email">Email: <?=$user['email']?></p>
        		<p class="address">Address: <?=$user['street_1']?>, <?=$user['street_2']?></p>
        		<p class="address"><?=$user['city']?> - <?=$user['province']?> - <?=$user['postal_code']?></p>        		       		
        	</div>
        	
        	<div id="cart">
        	  <table class="cart">
                <tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Qty</th>
                  <th>Sub</th>
                </tr>
        	   <?php foreach($cart as $row) : ?>
               <tr>
                  	<td><?=$row['name']?></td>
                    	<td>$<?=$row['price']?></td>
                    	<td><?=$row['qty']?></td>
                    	<td>$<?=$row['line_total']?></td>
                  	</tr>
                  <?php endforeach; ?>
                  <tr>
                 	<td>Subtotal</td>
                 	<td>-</td>
                 	<td>-</td>
                 	<td>$<?=$subtotal?></td>
                 </tr>
                 <tr>
                 	<td>GST</td>
                 	<td>-</td>
                 	<td>-</td>
                 	<td>$<?=number_format($GST, 2)?></td>
                 </tr>
                  <tr>
                 	<td>PST</td>
                 	<td>-</td>
                 	<td>-</td>
                 	<td>$<?=number_format($PST, 2)?></td>
                 </tr>
                 <tr>
                 	<td>TOTAL</td>
                 	<td>-</td>
                 	<td>-</td>
                 	<td>$<?=number_format($total, 2)?></td>
                 </tr>
                </table>
        	</div>
        </div>


        
      </div>  
      <!-- End of Content -->
      
<?php

		include 'inc/footer_inc.php';
 ?>